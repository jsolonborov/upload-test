const express = require('express');
const app = express();

app.use(express.static('public'))

app.post('/upload', (req, res) => {
    res
        .status(415)
        .send('Unsupported media type')
})

app.listen(3001, () => console.log('started'))
