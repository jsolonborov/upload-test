const fileInput = document.getElementById('input')
const uploadButton = document.getElementById('upload-button')

fileInput.addEventListener('change', () => {
    const file = fileInput.files[0]
    console.log(file)
})

uploadButton.addEventListener('click', () => {
    if (!fileInput.files[0]) {
        return;
    }

    uploadFile(fileInput.files[0])
})


function uploadFile(file) {
    const reader = new FileReader();
    const xhr = new XMLHttpRequest();

    xhr.onreadystatechange = (event) => {
        console.log('Response status code: ', event.currentTarget.status)
        console.log('Response text: ', event.currentTarget.response)

        document.getElementById('error').innerHTML = `${event.currentTarget.status}: ${event.currentTarget.response}`
    }

    reader.onload = function(evt) {
        xhr.send(evt.target.result);
    };
    reader.readAsBinaryString(file);
    xhr.open('POST', '/upload');
}
